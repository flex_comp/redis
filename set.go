package redis

import (
	"github.com/gomodule/redigo/redis"
)

func SAdd(key string, fields ...interface{}) error {
	return ins.SAdd(key, fields...)
}

func (r *Redis) SAdd(key string, fields ...interface{}) error {
	c := r.pool.Get()
	defer c.Close()

	_, err := c.Do("SADD", append([]interface{}{key}, fields...)...)
	return err
}

func SCard(key string) (int, error) {
	return ins.SCard(key)
}

func (r *Redis) SCard(key string) (int, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Int(c.Do("SCARD", key))
}

func SDiff(keys ...interface{}) ([]string, error) {
	return ins.SDiff(keys...)
}

func (r *Redis) SDiff(keys ...interface{}) ([]string, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Strings(c.Do("SDIFF", keys...))
}

func SDiffStore(dstKey string, keys ...interface{}) (int, error) {
	return ins.SDiffStore(dstKey, keys...)
}

func (r *Redis) SDiffStore(dstKey string, keys ...interface{}) (int, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Int(c.Do("SDIFFSTORE", append([]interface{}{dstKey}, keys...)...))
}

func SInter(keys ...interface{}) ([]string, error) {
	return ins.SInter(keys...)
}

func (r *Redis) SInter(keys ...interface{}) ([]string, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Strings(c.Do("SINTER", keys...))
}

func SInterStore(dstKey string, keys ...interface{}) (int, error) {
	return ins.SInterStore(dstKey, keys...)
}

func (r *Redis) SInterStore(dstKey string, keys ...interface{}) (int, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Int(c.Do("SINTERSTORE", append([]interface{}{dstKey}, keys...)...))
}

func SIsMember(key string, member interface{}) (bool, error) {
	return ins.SIsMember(key, member)
}

func (r *Redis) SIsMember(key string, member interface{}) (bool, error) {
	c := r.pool.Get()
	defer c.Close()

	cnt, err := redis.Int(c.Do("SISMEMBER", key, member))
	return cnt == 1, err
}

func SMembers(key string) ([]string, error) {
	return ins.SMembers(key)
}

func (r *Redis) SMembers(key string) ([]string, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Strings(c.Do("SMEMBERS", key))
}

func SMove(srcKey, dstKey string, member interface{}) (bool, error) {
	return ins.SMove(srcKey, dstKey, member)
}

func (r *Redis) SMove(srcKey, dstKey string, member interface{}) (bool, error) {
	c := r.pool.Get()
	defer c.Close()

	ok, err := redis.Int(c.Do("SMOVE", srcKey, dstKey, member))
	return ok == 1, err
}

func SPop(key string, cnt int) ([]string, error) {
	return ins.SPop(key, cnt)
}

func (r *Redis) SPop(key string, cnt int) ([]string, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Strings(c.Do("SPOP", key, cnt))
}

func SRandMember(key string, cnt int) ([]string, error) {
	return ins.SRandMember(key, cnt)
}

func (r *Redis) SRandMember(key string, cnt int) ([]string, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Strings(c.Do("SRandMember", key, cnt))
}

func SRem(key string, members ...interface{}) error {
	return ins.SRem(key, members...)
}

func (r *Redis) SRem(key string, members ...interface{}) error {
	c := r.pool.Get()
	defer c.Close()

	_, err := c.Do("SREM", append([]interface{}{key}, members...)...)
	return err
}

func SUnion(keys ...interface{}) ([]string, error) {
	return ins.SUnion(keys...)
}

func (r *Redis) SUnion(keys ...interface{}) ([]string, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Strings(c.Do("SUNION", keys...))
}

func SUnionStore(dstKey string, keys ...interface{}) (int, error) {
	return ins.SUnionStore(dstKey, keys...)
}

func (r *Redis) SUnionStore(dstKey string, keys ...interface{}) (int, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Int(c.Do("SUNIONSTORE", append([]interface{}{dstKey}, keys...)...))
}
