package redis

import (
	"github.com/gomodule/redigo/redis"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"regexp"
	"strings"
)

func (r *Redis) loadScript(dir string) {
	files, err := util.LoadFromDir(dir, "lua")
	if err != nil {
		log.Error("加载失败:", err)
		return
	}

	reg, _ := regexp.Compile("^--number of keys:[0-9]{1,3}")

	for name, content := range files {
		str := reg.FindString(content)
		str = strings.Split(str, ":")[1]
		keys := util.ToInt(str)
		log.Debug("发现脚本:", name, ", keys:", keys)

		r.scripts[name] = redis.NewScript(keys, content)
	}
}

func LoadScriptRaw(name string, keys int, content string) {
	ins.LoadScriptRaw(name, keys, content)
}

func (r *Redis) LoadScriptRaw(name string, keys int, content string) {
	r.scripts[name] = redis.NewScript(keys, content)
}

func Script(name string, args ...interface{}) (interface{}, error) {
	return ins.Script(name, args...)
}

func (r *Redis) Script(name string, args ...interface{}) (interface{}, error) {
	s, ok := r.scripts[name]
	if !ok {
		return nil, ErrNotFoundScript
	}

	c := r.pool.Get()
	defer c.Close()

	return s.Do(c, args...)
}
