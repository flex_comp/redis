module gitlab.com/flex_comp/redis

go 1.16

require (
	github.com/gomodule/redigo v1.8.6
	github.com/spf13/viper v1.10.1 // indirect
	gitlab.com/flex_comp/comp v0.1.4
	gitlab.com/flex_comp/conf v0.1.0
	gitlab.com/flex_comp/log v0.1.6
	gitlab.com/flex_comp/remote_conf v0.1.2 // indirect
	gitlab.com/flex_comp/util v0.2.6
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.19.1 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
)
