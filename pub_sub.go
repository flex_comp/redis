package redis

import (
	"github.com/gomodule/redigo/redis"
	"gitlab.com/flex_comp/log"
	"sync/atomic"
)

func Pub(key string, val string) (int, error) {
	return ins.Pub(key, val)
}

func (r *Redis) Pub(key string, val string) (int, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("PUBLISH", key, val)
	count, err := redis.Int(rep, err)
	if err != nil {
		log.ErrorF("PUBLISH %s -> %v 失败: %s", key, val, err)
		return 0, err
	}

	return count, nil
}

func Sub(key string) (chan string, int64) {
	return ins.Sub(key)
}

// Sub 请在组件Init时调用, Start之后调用无效
func (r *Redis) Sub(key string) (chan string, int64) {
	i64 := atomic.AddInt64(&r.incr, 1)
	ch := make(chan string, 1)

	m, ok := r.subContainer[key]
	if !ok {
		m = make(map[int64]chan string)
		r.subContainer[key] = m
	}

	m[i64] = ch
	return ch, i64
}
