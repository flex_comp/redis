package redis

import "errors"

var (
	ErrWrongArgs      = errors.New("wrong arguments")
	ErrNotFoundScript = errors.New("not found script")
)
