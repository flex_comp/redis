package redis

import (
	"gitlab.com/flex_comp/comp"
	"testing"
	"time"
)

func TestRedis(t *testing.T) {
	t.Run("test_redis", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{
			"etcd_hosts": []string{"http://192.168.1.103:2379"},
		})

		ch := make(chan bool, 1)
		go func() {
			defer func() {
				ch <- true
			}()

			<-time.After(time.Second)
			//n := time.Now()
			t.Log(Script("get"))
			//s := time.Now().Sub(n)
			//t.Log("login_conn_reg, 执行时间:", s)
			//idx := 0
			//
			//for range time.Tick(time.Millisecond * 10) {
			//	id := fmt.Sprintf("%d%s", idx+10000000, "abcdefghijklmnopqrstuvwxyzabcdefg")
			//	Script("login_conn_reg", "login:conn:mgr:agent:3", "login:conn:counter", id)
			//	idx++
			//
			//	if idx == 3000 {
			//		t.Log("插入3000条数据")
			//		return
			//	}
			//	//atomic.AddInt32(&idx, 1)
			//	//n := time.Now()
			//	//Script("login_conn_reg", "login:conn:mgr:agent:3", "login:conn:counter", "123456789abcde")
			//	//s := time.Now().Sub(n)
			//	//
			//	//t.Log("login_conn_reg, 执行时间:", s)
			//	//
			//	//<-time.After(time.Second)
			//	//
			//	//n = time.Now()
			//	//Script("login_conn_unreg", "login:conn:mgr:agent:3", "login:conn:counter", "disconnect", "123456789abcde")
			//	//s = time.Now().Sub(n)
			//	//
			//	//t.Log("login_conn_unreg, 执行时间:", s)
			//}

			// login:conn:mgr:agent3 login:conn:counter 12345689abcdefg
			//if _, err := Set("foo", "bar"); err != nil {
			//	t.Fail()
			//	return
			//}
			//
			//v, err := Get("foo")
			//if err != nil || util.ToString(v) != "bar" {
			//	t.Fail()
			//	return
			//}
			//
			//t.Log("foo:", util.ToString(v))
			//
			//if err := Del("foo"); err != nil {
			//	t.Fail()
			//	return
			//}
		}()

		_ = comp.Start(ch)
	})
}
