package redis

import (
	"github.com/gomodule/redigo/redis"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
)

func HExists(key, field string) (bool, error) {
	return ins.HExists(key, field)
}

func (r *Redis) HExists(key, field string) (bool, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("HEXISTS", key, field)
	i, err := redis.Int(rep, err)
	return i == 1, err
}

func HDel(key string, fields ...interface{}) (int, error) {
	return ins.HDel(key, fields...)
}

func (r *Redis) HDel(key string, fields ...interface{}) (int, error) {
	if len(fields) == 0 {
		log.WarnF("hdel %s -> %v, 字段为空", key, fields)
		return 0, ErrWrongArgs
	}

	c := r.pool.Get()
	defer c.Close()

	args := append([]interface{}{key}, fields...)
	rep, err := c.Do("HDEL", args...)
	return redis.Int(rep, err)
}

func HMSet(key string, pairs ...interface{}) error {
	return ins.HMSet(key, pairs...)
}

func (r *Redis) HMSet(key string, pairs ...interface{}) error {
	if len(pairs) == 0 {
		log.WarnF("hmset %s -> %v, 字段为空", key, pairs)
		return ErrWrongArgs
	}

	if len(pairs)%2 != 0 {
		log.WarnF("hmset %s -> %v, 字段不成对", key, pairs)
		return ErrWrongArgs
	}

	args := append([]interface{}{key}, pairs...)

	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("HMSET", args...)
	if err != nil && rep != "OK" {
		log.ErrorF("hmset %s -> %v, 失败: %s - %s", key, pairs, rep, err)
	}

	return err
}

func HMGet(key string, fields ...interface{}) (map[string]string, error) {
	return ins.HMGet(key, fields...)
}

func (r *Redis) HMGet(key string, fields ...interface{}) (map[string]string, error) {
	if len(fields) == 0 {
		log.WarnF("hmget %s -> %v, 字段为空", key, fields)
		return nil, ErrWrongArgs
	}

	args := append([]interface{}{key}, fields...)

	c := r.pool.Get()
	rep, err := c.Do("HMGET", args...)
	c.Close()

	s, err := redis.Strings(rep, err)
	if err != nil {
		log.ErrorF("hmget %s -> %v, 失败: %s", key, fields, err)
		return nil, err
	}

	m := make(map[string]string)
	idx := 0
	for _, field := range fields {
		if idx >= len(s) {
			log.WarnF("hmget %s -> %v, 获取值与Field不对等: %v", key, fields, s)
			return m, nil
		}

		m[util.ToString(field)] = s[idx]
		idx++
	}

	return m, nil
}

func HGetAll(key string) (map[string]string, error) {
	return ins.HGetAll(key)
}

func (r *Redis) HGetAll(key string) (map[string]string, error) {
	c := r.pool.Get()
	rep, err := c.Do("HGETALL", key)
	c.Close()

	s, err := redis.Strings(rep, err)
	if err != nil {
		log.ErrorF("hgetall %s, 失败: %s", key, err)
		return nil, err
	}

	if len(s)%2 != 0 {
		log.WarnF("hgetall %s, 结果不成对: %v", key, s)
	}

	m := make(map[string]string)
	pairs := len(s) / 2
	for i := 0; i < pairs; i++ {
		f := s[i*2]
		v := s[i*2+1]
		m[f] = v
	}

	return m, nil
}

func HGetAllSlice(key string) ([]string, error) {
	return ins.HGetAllSlice(key)
}

func (r *Redis) HGetAllSlice(key string) ([]string, error) {
	c := r.pool.Get()
	rep, err := c.Do("HGETALL", key)
	c.Close()

	s, err := redis.Strings(rep, err)
	if err != nil {
		log.ErrorF("hgetall %s, 失败: %s", key, err)
		return nil, err
	}

	return s, nil
}

func HIncrBy(key string, field string, incr int) (int, error) {
	return ins.HIncrBy(key, field, incr)
}

func (r *Redis) HIncrBy(key string, field string, incr int) (int, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("HINCRBY", key, field, incr)
	return redis.Int(rep, err)
}

func HKeys(key string) ([]string, error) {
	return ins.HKeys(key)
}

func (r *Redis) HKeys(key string) ([]string, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("HKEYS", key)
	return redis.Strings(rep, err)
}

func HVals(key string) ([]string, error) {
	return ins.HVals(key)
}

func (r *Redis) HVals(key string) ([]string, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("HVALS", key)
	return redis.Strings(rep, err)
}

func HLen(key string) (int, error) {
	return ins.HLen(key)
}

func (r *Redis) HLen(key string) (int, error) {
	c := r.pool.Get()
	defer c.Close()

	return redis.Int(c.Do("HLEN", key))
}
