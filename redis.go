package redis

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/remote_conf"
	"gitlab.com/flex_comp/util"
)

var (
	ins *Redis
)

func init() {
	ins = &Redis{
		subContainer: make(map[string]map[int64]chan string),
		scripts:      make(map[string]*redis.Script),
	}

	_ = comp.RegComp(ins)
}

type Redis struct {
	pool *redis.Pool

	subContainer map[string]map[int64]chan string
	scripts      map[string]*redis.Script

	incr int64
}

func (r *Redis) Init(map[string]interface{}, ...interface{}) error {
	host := util.ToString(conf.Get("redis", "host"))
	port := util.ToInt(conf.Get("redis", "port"))
	user := util.ToString(conf.Get("redis", "user"))
	passwd := util.ToString(conf.Get("redis", "passwd"))
	dir := util.ToString(conf.Get("redis", "script.dir"))

	if len(dir) > 0 {
		r.loadScript(dir)
	}

	addr := fmt.Sprintf("%s:%d", host, port)

	fnDial := func() (redis.Conn, error) {
		return redis.Dial("tcp", addr, redis.DialUsername(user), redis.DialPassword(passwd))
	}

	r.pool = &redis.Pool{
		Dial:      fnDial,
		MaxIdle:   64,
		MaxActive: 10240,
	}

	return nil
}

func (r *Redis) Start(_ ...interface{}) error {
	var keys []interface{}
	for s := range r.subContainer {
		keys = append(keys, s)
	}

	if len(keys) == 0 {
		return nil
	}

	go func() {
		for {
			c := r.pool.Get()
			psc := redis.PubSubConn{Conn: c}

			_ = psc.Subscribe(keys...)
			for c.Err() == nil {
				switch v := psc.Receive().(type) {
				case redis.Message:
					m, ok := r.subContainer[v.Channel]
					if !ok {
						continue
					}

					data := string(v.Data)
					for _, ch := range m {
						ch <- data
					}
				case redis.Subscription:
					log.InfoF("订阅信息: %s: %s %d", v.Channel, v.Kind, v.Count)
				case error:
					log.Error("订阅连接错误:", v)
				}
			}

			_ = c.Close()
		}
	}()

	return nil
}

func (r *Redis) UnInit() {
	_ = r.pool.Close()
}

func (r *Redis) Name() string {
	return "redis"
}
