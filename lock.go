package redis

import (
	"time"
)

func Lock(key string, dur time.Duration) (bool, error) {
	return ins.Lock(key, dur)
}

func (r *Redis) Lock(key string, dur time.Duration) (bool, error) {
	return r.SetNXExpire(key, "", dur)
}
