package redis

import (
	"github.com/gomodule/redigo/redis"
	"gitlab.com/flex_comp/log"
	"time"
)

func Del(key interface{}, keys ...interface{}) error {
	return ins.Del(key, keys...)
}

func (r *Redis) Del(key interface{}, keys ...interface{}) error {
	c := r.pool.Get()
	defer c.Close()

	_, err := c.Do("DEL", append([]interface{}{key}, keys)...)
	if err != nil {
		log.Error(err)
	}

	return err
}

func Get(key string) (interface{}, error) {
	return ins.Get(key)
}

func (r *Redis) Get(key string) (interface{}, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("GET", key)
	if err != nil && err != redis.ErrNil {
		log.ErrorF("get %s 失败: %s", key, err)
		return nil, err
	}

	return rep, nil
}

func Set(key string, val interface{}) (bool, error) {
	return ins.Set(key, val)
}

func (r *Redis) Set(key string, val interface{}) (bool, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("SET", key, val)
	if err != nil && rep != "OK" {
		log.ErrorF("set %s -> %v 失败: %s, %s", key, val, rep, err)
		return false, err
	}

	return true, nil
}

func SetXX(key string, val interface{}) (bool, error) {
	return ins.SetXX(key, val)
}

func (r *Redis) SetXX(key string, val interface{}) (bool, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("SET", key, val, "XX")
	if err != nil && rep != "OK" {
		log.ErrorF("set xx %s -> %v 失败: %s, %s", key, val, rep, err)
		return false, err
	}

	return true, nil
}

func SetNX(key string, val interface{}) (bool, error) {
	return ins.SetNX(key, val)
}

func (r *Redis) SetNX(key string, val interface{}) (bool, error) {
	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("SET", key, val, "NX")
	if err != nil && rep != "OK" {
		log.ErrorF("set nx %s -> %v 失败: %s, %s", key, val, rep, err)
		return false, err
	}

	return true, nil
}

func SetExpire(key string, val interface{}, dur time.Duration) (bool, error) {
	return ins.SetExpire(key, val, dur)
}

func (r *Redis) SetExpire(key string, val interface{}, dur time.Duration) (bool, error) {
	ms := int(dur / time.Millisecond)
	if ms <= 0 {
		log.ErrorF("set ex %s -> %v 失败, 最小精度为1ms: %v", key, val, dur)
		return false, ErrWrongArgs
	}

	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("SET", key, val, "PX", ms)
	if err != nil && rep != "OK" {
		log.ErrorF("set ex %s -> %v 失败: %s, %s", key, val, rep, err)
		return false, err
	}

	return true, nil
}

func SetXXExpire(key string, val interface{}, dur time.Duration) (bool, error) {
	return ins.SetXXExpire(key, val, dur)
}

func (r *Redis) SetXXExpire(key string, val interface{}, dur time.Duration) (bool, error) {
	ms := int(dur / time.Millisecond)
	if ms <= 0 {
		log.ErrorF("set xx ex %s -> %v 失败, 最小精度为1ms: %v", key, val, dur)
		return false, ErrWrongArgs
	}

	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("SET", key, val, "XX", "PX", ms)
	if err != nil && rep != "OK" {
		log.ErrorF("set xx ex %s -> %v 失败: %s, %s", key, val, rep, err)
		return false, err
	}

	return true, nil
}

func SetNXExpire(key string, val interface{}, dur time.Duration) (bool, error) {
	return ins.SetNXExpire(key, val, dur)
}

func (r *Redis) SetNXExpire(key string, val interface{}, dur time.Duration) (bool, error) {
	ms := int(dur / time.Millisecond)
	if ms <= 0 {
		log.ErrorF("set nx ex %s -> %v 失败, 最小精度为1ms: %v", key, val, dur)
		return false, ErrWrongArgs
	}

	c := r.pool.Get()
	defer c.Close()

	rep, err := c.Do("SET", key, val, "NX", "PX", ms)
	if err != nil && rep != "OK" {
		log.ErrorF("set nx ex %s -> %v 失败: %s, %s", key, val, rep, err)
		return false, err
	}

	return true, nil
}
